DESCRIPTION = "QTI datarmnet software"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://rmnet_ctl.h;md5=3efc6c06b4744a8d997e39fcd0edc224"

inherit module

FILESPATH =+ "${TOPDIR}/../common/datarmnet:"
FILESEXTRAPATHS_prepend := "${THISDIR}/:"

#RDEPENDS_${PN} = "virtual/kernel"
DEPENDS+="virtual/kernel"

SRC_URI = "file://core \
	"
inherit systemd

S = "${WORKDIR}/core"

EXTRA_OEMAKE = '\
	EXTRA_CFLAGS="" \
'

PACKAGES += "kernel-module-rmnet-core"

do_configure() {
	${BASE_WORKDIR}/${MACHINE_ARCH}${HOST_VENDOR}-${HOST_OS}/linux-ipq/5.4-r0/kernel/scripts/config --file ${BASE_WORKDIR}/${MACHINE_ARCH}${HOST_VENDOR}-${HOST_OS}/linux-ipq/5.4-r0/build/.config --enable CONFIG_RMNET_CORE --enable CONFIG_RMNET_CTL
	cp ${S}/dfc.h ${STAGING_KERNEL_DIR}/include/trace/events/
	cp ${S}/rmnet_trace.h ${STAGING_KERNEL_DIR}/include/trace/events/
	cp ${S}/wda.h ${STAGING_KERNEL_DIR}/include/trace/events/
}

do_compile() {
	unset LDFLAGS
	make CROSS_COMPILE=${TARGET_PREFIX} ARCH=${KARCH} KERNEL_SRC=${BASE_WORKDIR}/${MACHINE_ARCH}${HOST_VENDOR}-${HOST_OS}/linux-ipq/5.4-r0/build/
}

do_install_append() {
	install -d ${D}${base_libdir}/modules/${KERNEL_VERSION}/kernel/drivers/${PN}
	install -m 0644 ${S}/rmnet_core.ko ${D}${base_libdir}/modules/${KERNEL_VERSION}/kernel/drivers/${PN}
	install -m 0644 ${S}/rmnet_ctl.ko ${D}${base_libdir}/modules/${KERNEL_VERSION}/kernel/drivers/${PN}
}

do_patch() {
	cd ${S}/..
	patch -p1 -t < ${THISDIR}/patches/datarmnet.patch
	cd -
}

FILES_${PN} = " \
"

INSANE_SKIP_${PN} += " ldflags"
INHIBIT_PACKAGE_STRIP = "1"
INHIBIT_SYSROOT_STRIP = "1"
SOLIBS = ".so"
FILES_SOLIBSDEV = ""
