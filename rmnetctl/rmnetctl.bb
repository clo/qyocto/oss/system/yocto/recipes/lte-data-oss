DESCRIPTION = "QTI rmnetctl software"
LICENSE = "BSD-3-clause"
LIC_FILES_CHKSUM = "file://inc/librmnetctl.h;md5=bf93f341a92c3301e69ea4221218576d"

FILESPATH =+ "${TOPDIR}/../common/data-oss:"
FILESEXTRAPATHS_prepend := "${THISDIR}/:"

RDEPENDS_${PN} = "bash"
DEPENDS+="virtual/kernel"

SRC_URI = "file://rmnetctl \
	"
inherit systemd

S = "${WORKDIR}/rmnetctl"

EXTRA_OEMAKE = '\
	EXTRA_CFLAGS+="	-I${BASE_WORKDIR}/${MACHINE_ARCH}${HOST_VENDOR}-${HOST_OS}/linux-ipq/5.4-r0/build/usr/include \
	" \
'

do_install() {
	ls -lh
	install -d  ${D}/usr/lib
	install -d  ${D}/usr/bin
	install -m 0644 ${S}/lib/librmnetctl.so ${D}/usr/lib
	install -m 0755 ${S}/bin/rmnetcli ${D}/usr/bin
}
do_patch() {
	cd ${S}/..
	patch -p1 -t < ${THISDIR}/patches/rmnetctl.patch
	cd -
}
FILES_${PN} = " \
	/usr/lib/librmnetctl.so \
"

INSANE_SKIP_${PN} += " ldflags"
INHIBIT_PACKAGE_STRIP = "1"
INHIBIT_SYSROOT_STRIP = "1"
SOLIBS = ".so"
FILES_SOLIBSDEV = ""
